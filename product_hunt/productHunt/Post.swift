import SwiftyJSON

class Post {
    
    var id: Int
    var name: String
    var tagline: String
    var redirectUrl: String
    var upvotes: Int
    var thumbnail: Thumbnail
    var screenshot: Screenshot
    
    init?(json: JSON) {
        guard let id = json["id"].int,
              let name = json["name"].string,
              let tagline = json["tagline"].string,
              let redirectUrl = json["redirect_url"].string,
              let upvotes = json["votes_count"].int,
              let thumbnail = Thumbnail(json: json["thumbnail"]),
              let screenshot = Screenshot(json: json["screenshot_url"]) else {
            log.error("Create post error")
            return nil
        }
        self.id = id
        self.name = name
        self.tagline = tagline
        self.redirectUrl = redirectUrl
        self.upvotes = upvotes
        self.thumbnail = thumbnail
        self.screenshot = screenshot
    }
    
}

class Thumbnail {
    var id: Int
    var mediaType: String
    var url: String
    
    init?(json: JSON) {
        guard let id = json["id"].int,
            let mediaType = json["media_type"].string,
            let url = json["image_url"].string else {
                log.error("Create thumbnail error")
                return nil
        }
        self.id = id
        self.mediaType = mediaType
        self.url = url
    }
}

class Screenshot {
    
    var image300: String
    var image850: String
    
    init?(json: JSON) {
        guard let img300 = json["300px"].string,
            let img850 = json["850px"].string else {
                log.error("Create screenshot error")
                return nil
        }
        self.image300 = img300
        self.image850 = img850
    }
    
}
