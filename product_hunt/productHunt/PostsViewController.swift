import UIKit
import UserNotifications

class PostsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {

    var listCategories: [Category] = []
    var listPosts: [Post] = []
    
    var alertView: UIView!
    var alertHeader: UILabel!
    var alertPick: UIPickerView!
    var alertOk: UIButton!
    var selectCategory: Category?
    
    var alertLoading: UIAlertController!
    
    //MARK: IBOutlet
    @IBOutlet weak var posts: UITableView!
    @IBOutlet weak var navigationCategory: UINavigationItem!
    
    //MARK: ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertLoading = UIAlertController(title: "Загрузка", message: "Пожалуйста подождите", preferredStyle: .alert)
        present(alertLoading, animated: true)
        
        posts.delegate = self
        posts.dataSource = self
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .allEvents)
        posts.refreshControl = refreshControl
        api.getListCategories() { result, code, list in
            if result && list.count > 0 {
                self.listCategories = list
                self.navigationCategory.title = list[0].name
                api.selectedCategory = list[0]
                self.refresh()
            }
        }
        
        // Инициализируем AlertPicker
        let x = view.frame.width - view.frame.width * 90 / 100
        let y = view.frame.height - view.frame.height * 90 / 100
        let width = view.frame.width * 80 / 100
        let height = view.frame.height * 80 / 100
        log.debug("Alert picked frame: x=\(x) y=\(y) width=\(width) height=\(height)")
        alertView = UIView(frame: CGRect(x: x, y: y, width: width, height: height))
        alertView.backgroundColor = UIColor.white
        alertView.layer.cornerRadius = 15
        alertHeader = UILabel(frame: CGRect(x: 8, y: 8, width: width - 16, height: 16))
        alertHeader.text = "Выбирете категорию"
        alertHeader.textAlignment = .center
        alertView.addSubview(alertHeader)
        alertPick = UIPickerView(frame: CGRect(x: 8, y: 44, width: width - 16, height: height - 160))
        alertPick.delegate = self
        alertPick.dataSource = self
        alertView.addSubview(alertPick)
        alertOk = UIButton(type: .system)
        alertOk.frame = CGRect(x: 8, y: height - 32, width: width - 16, height: 24)
        alertOk.setTitle("Сменить категорию", for: .normal)
        alertOk.addTarget(self, action: #selector(touchConfirmChangeCategory), for: .touchUpInside)
        alertView.addSubview(alertOk)
        alertView.isHidden = true
        view.addSubview(alertView)
        
        // Настроим выполнение notification
        scheduleNotification(inMinutes: TimeInterval(updateNotificationInMinutes))
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? PostInfoViewController, let post = sender as? Post {
            log.debug("Prepare for PostInfoViewController")
            controller.post = post
        }
    }
    
    // MARK: My
    func refresh() {
        log.debug("Start refresh")
        api.getListPosts() { result, code, list in
            log.debug("Get response: \(result) and list count: \(list.count)")
            if result {
                self.listPosts = list
                DispatchQueue.main.async {
                    self.posts.reloadData()
                    self.posts.refreshControl?.endRefreshing()
                    self.alertLoading.dismiss(animated: true)
                }
                log.debug("End refresh")
            }
        }
    }
    
    func touchConfirmChangeCategory() {
        api.selectedCategory = selectCategory!
        alertView.isHidden = true
        refresh()
    }
    
    //MARK: TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listPosts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "post") as? PostTableViewCell {
            cell.refreshCell(listPosts[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "postToDetail", sender: listPosts[indexPath.row])
    }
    
    //MARK: Picker
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listCategories[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listCategories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        log.debug("Selected picker: \(listCategories[row].name)")
        selectCategory = listCategories[row]
    }
    
    //MARK: IBAction
    @IBAction func touchListCategory(_ sender: Any) {
        if listCategories.count == 0 {
            return
        }
        alertView.isHidden = false
        alertPick.reloadAllComponents()
    }
    
    //MARK: Notification
    func scheduleNotification(inMinutes minutes: TimeInterval) {
        DispatchQueue.global(qos: .background).async {
            log.debug("Start notification")
            let date = Date(timeIntervalSinceNow: minutes * 60)
            api.getListPosts { (result, code, list) in
                log.debug("Notification API get request: \(result)")
                if result {
                    var newPost: [Post] = []
                    var dictionaryPost = self.getDictionaryPosts()
                    for post in newPost {
                        if dictionaryPost.removeValue(forKey: post.id) == nil {
                            newPost.append(post)
                        }
                    }
                    log.debug("Notification new posts: \(newPost.count)")
                    if newPost.count > 0 {
                        let content = UNMutableNotificationContent()
                        let calendar = Calendar(identifier: .gregorian)
                        let components = calendar.dateComponents([.hour, .minute, .second], from: date)
                        if newPost.count > 1 {
                            content.title = "Новые посты"
                            content.body = "В категории \(self.selectCategory?.name ?? "Tech") - \(newPost.count) новых постов"
                        } else {
                            content.title = "Новый пост"
                            content.body = "\(newPost[0].name) - \(newPost[0].tagline)"
                        }
                        let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
                        let request = UNNotificationRequest(identifier: "NewPost", content: content, trigger: trigger)
                        let center = UNUserNotificationCenter.current()
                        center.add(request, withCompletionHandler: nil)
                    }
                }
            }
            log.debug("Sleep")
            sleep(UInt32(minutes * 60))
            log.debug("Repead notification")
            self.scheduleNotification(inMinutes: TimeInterval(updateNotificationInMinutes))
            log.debug("Exit notification")
        }
    }
    
    func getDictionaryPosts() -> [Int: Post] {
        var dictionary: [Int: Post] = [:]
        for post in listPosts {
            dictionary[post.id] = post
        }
        return dictionary
    }
}
