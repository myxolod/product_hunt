import SwiftyJSON

class Category {
    var id: Int
    var slug: String
    var name: String
    var color: String
    var itemName: String
    
    init?(json: JSON) {
        guard let id = json["id"].int,
              let slug = json["slug"].string,
              let name = json["name"].string,
              let color = json["color"].string,
              let item = json["item_name"].string else {
            log.error("Error create Category. Json: \(json)")
            return nil
        }
        self.id = id
        self.slug = slug
        self.name = name
        self.color = color
        self.itemName = item
    }
    
    init(id: Int, slug: String) {
        self.id = id
        self.slug = slug
        self.name = slug
        self.color = ""
        self.itemName = ""
    }
}
