//
//  WebViewController.swift
//  productHunt
//
//  Created by Сергей Мельников on 03/07/2017.
//  Copyright © 2017 Сергей Мельников. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    var url: String!

    //MARK: IBOutlet
    @IBOutlet weak var web: UIWebView!
    
    override func viewDidAppear(_ animated: Bool) {
        if let successUrl = URL(string: url) {
            web.loadRequest(URLRequest(url: successUrl))
        }
    }

}
