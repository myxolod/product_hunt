import UIKit
import SDWebImage

class PostTableViewCell: UITableViewCell {
    
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var tagline: UILabel!
    @IBOutlet weak var upvotes: UILabel!

    var post: Post!
    
    func refreshCell(_ post: Post) {
        self.post = post
        name.text = post.name
        tagline.text = post.tagline
        upvotes.text = String(post.upvotes)
        self.thumbnail.sd_setImage(with: URL(string: post.thumbnail.url), placeholderImage: #imageLiteral(resourceName: "ic_not_interested"))
    }

}
