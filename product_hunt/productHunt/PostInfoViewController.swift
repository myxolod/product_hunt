import UIKit
import SDWebImage

class PostInfoViewController: UIViewController {

    var post: Post!
    
    //MARK: IBOutlet
    @IBOutlet weak var screenshot: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var upvotes: UILabel!
    @IBOutlet weak var tagline: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        log.debug("Post screenshot: " + post.screenshot.image300)
        screenshot.sd_setImage(with: URL(string: post.screenshot.image300), placeholderImage: #imageLiteral(resourceName: "ic_not_interested"))
        name.text = post.name
        upvotes.text = String(post.upvotes)
        tagline.text = post.tagline
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? WebViewController, let url = sender as? String {
            controller.url = url
        }
    }
    
    //MARK: IBAction
    @IBAction func touchGetIt(_ sender: Any) {
        performSegue(withIdentifier: "detailToWeb", sender: post.redirectUrl)
    }
    

}
