import UIKit

typealias CallbackCategory = (_ result: Bool, _ code: ResponseCode, _ list: [Category]) -> ()
typealias CallbackItem = (_ result: Bool, _ code: ResponseCode, _ list: [Post]) -> ()

/// Константа времени выполнения обновления списка в background
let updateNotificationInMinutes = 60.0

enum ResponseCode {
    case errorNetwork, errorGetStatusCode, errorJson, errorIncorrectSettings, success
}

/// Расширение на скачивание картинок
extension UIImageView {
    
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            log.debug("Downloaded image success")
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        log.debug("Start downloaded image")
        downloadedFrom(url: url, contentMode: mode)
    }
}
