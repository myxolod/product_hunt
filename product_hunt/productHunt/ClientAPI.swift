import Foundation
import SwiftHTTP
import SwiftyBeaver
import SwiftyJSON

class ClientAPI {
    
    private var token = "591f99547f569b05ba7d8777e2e0824eea16c440292cce1f8dfb3952cc9937ff"
    private let domen = "https://api.producthunt.com/"
    private let category = "v1/categories"
    private let post = "/posts"
    var selectedCategory: Category?
    
    var key: String {
        get {
            return "?access_token=\(token)"
        }
        set {
            token = newValue
        }
    }
    
/// Функция получения списка категорий
    func getListCategories(callback: @escaping CallbackCategory) {
        do {
            log.debug("HTTP.GET: \(domen + category + key)")
            try HTTP.GET(domen + category + key).start() { response in
                guard response.statusCode != nil else {
                    callback(false, ResponseCode.errorGetStatusCode, [])
                    return
                }
                guard let categoriesJSON = JSON(data: response.data)["categories"].array else {
                    callback(false, ResponseCode.errorJson, [])
                    return
                }
                var categories: [Category] = []
                for categoryJSON in categoriesJSON {
                    if let category = Category(json: categoryJSON) {
                        categories.append(category)
                    }
                }
                callback(true, ResponseCode.success, categories)
            }
        } catch let error {
            log.error("Network error: \(error.localizedDescription)")
            callback(false, ResponseCode.errorNetwork, [])
        }
    }
    
/// Функция получения списка постов в указаной (selectedCategory) категории.
    func getListPosts(callback: @escaping CallbackItem) {
        guard selectedCategory != nil else {
            log.warning("Selected category is nil")
            callback(false, ResponseCode.errorIncorrectSettings, [])
            return
        }
        do {
            log.debug("HTTP.GET: \(domen + category + "/" + selectedCategory!.slug + post + key)")
            try HTTP.GET(domen + category + "/" + selectedCategory!.slug + post + key).start() { response in
                guard response.statusCode != nil else {
                    callback(false, ResponseCode.errorGetStatusCode, [])
                    return
                }
                guard let postsJSON = JSON(data: response.data)["posts"].array else {
                    callback(false, ResponseCode.errorJson, [])
                    return
                }
                var posts: [Post] = []
                for postJSON in postsJSON {
                    if let post = Post(json: postJSON) {
                        posts.append(post)
                    }
                }
                callback(true, ResponseCode.success, posts)
            }
        } catch let error {
            log.error("Network error: \(error.localizedDescription)")
            callback(false, ResponseCode.errorNetwork, [])
        }
    }
}
