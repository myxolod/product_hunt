//
//  productHuntTests.swift
//  productHuntTests
//
//  Created by Сергей Мельников on 03/07/2017.
//  Copyright © 2017 Сергей Мельников. All rights reserved.
//

import XCTest
@testable import productHunt

class productHuntTests: XCTestCase {
    
    var api = ClientAPI()
    
    override func setUp() {
        super.setUp()
        api = ClientAPI()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetListCategories() {
        let exp = expectation(description: "\(#function)\(#line)")
        api.getListCategories() { result, code, list in
            XCTAssertTrue(result)
            XCTAssertTrue(list.count > 0)
            exp.fulfill()
        }
        waitForExpectations(timeout: 5) { (error) in
            if error != nil {
                XCTAssertTrue(false)
            }
        }
    }
    
    func testGetListCategoriesWithBadKey() {
        api.key = "123"
        let exp = expectation(description: "\(#function)\(#line)")
        api.getListCategories() { result, code, list in
            XCTAssertFalse(result)
            exp.fulfill()
        }
        waitForExpectations(timeout: 5) { (error) in
            if error != nil {
                XCTAssertTrue(false)
            }
        }
    }
    
    func testGetListPost() {
        let exp = expectation(description: "\(#function)\(#line)")
        api.selectedCategory = Category(id: 1, slug: "tech")
        api.getListPosts() { result, code, list in
            XCTAssertTrue(result)
            exp.fulfill()
        }
        waitForExpectations(timeout: 5) { (error) in
            if error != nil {
                XCTAssertTrue(false)
            }
        }
    }
    
    func testGetListPostsWithBadCategory() {
        let exp = expectation(description: "\(#function)\(#line)")
        api.getListPosts() { result, code, list in
            XCTAssertFalse(result)
            exp.fulfill()
        }
        waitForExpectations(timeout: 5) { (error) in
            if error != nil {
                XCTAssertTrue(false)
            }
        }
    }
    
}
